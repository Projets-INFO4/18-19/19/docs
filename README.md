# docs

*Monday 28 january :* 

Reading of a part of NIX's tutorial.
We have also installed NIX, and begin to test few commands, like create a default.nix with ocaml and coq.

*Monday 4 february :*

We talked with Olivier Richard. 
Read NIX's tutorial of HelloWorld. 

*Monday 11 february :*

Following of the reading of the NIX manual
First manipulation on the "hello" package

*Monday 18 february - Tuesday 19 february :* 

Following the discovery of NIX
Creation of the "hello" package. --See hello file
To build the package, yu just have to do "nix-build -A hello" on the workinprogress branch

*Monday 25 - Mardi 26 february :* 

Holidays

*Monday 4 march - Tuesday 5 march :* 

Reading of NUR manual.
First try to add our package on the NUR.
Permision problems. Discussion with the NUR community to resolve the issue.
Second try to push our repo on the NUR.

*Monday 11 march :*

Add of the repo on NUR : Success
Creation of the "PF" package with ocaml on the NUR.

*Tuesday 12 march :*

Trying to add two package in the same, here : setup of helloworld and ocaml in "PF"
Prepartion of the presentation.

*Monday 18 march :*

Preparation of the presentation of our project.
And trying to add the tuareg package to the PF-LT environment. 

*Tuesday 19 march :*

Success of add the tuareg package.
Try to create the "lustreV4" package in order to create the ALM1 environment.

*Monday-Tuesday 25-26 march :*

Creation of tutorials.

*Monday 1 april :*

Beginning of the report and the final presentation.

